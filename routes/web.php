<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Auth::routes();


        Route::get('/login', [\App\Http\Controllers\Auth\LoginController::class, 'show'])->name('login');
        Route::post('/login', [\App\Http\Controllers\Auth\LoginController::class, 'authenticate'])->name('login.store');

        Route::group(['middleware' => ['auth:web']], function () {
            Route::get('/', function () {
                return view('home');
            });
            Route::post('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

        });

