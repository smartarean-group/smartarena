<?php

use App\Http\Controllers\API\v1\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\v1\InfosController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/register', [AuthController::class, 'register']);
Route::post('/confirm', [AuthController::class, 'confirm']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/resend', [AuthController::class, 'resend']);

Route::get('/regions', [InfosController::class, 'regionList']);
Route::get('/city/{region_id}', [InfosController::class, 'cityListByRegion']);

Route::group([
    'namespace' => 'API\v1',
    'middleware' => ['sessions', 'custom_auth'],
],  function() {

//    Route::post('/refresh', [AuthController::class, 'refresh']);
//    Route::get('/profile', [AuthController::class, 'profile']);
    Route::post('/logout', [AuthController::class, 'logout']);

    Route::apiResource('arena', 'ArenaApiController');
    Route::apiResource('player-api', 'PlayerApiController');
    Route::apiResource('stadion-owner', 'StadionOwnerApiController');
    Route::apiResource('rent', 'RentApiController');
    Route::apiResource('stadion', 'StadionApiController');
    Route::apiResource('team', 'TeamApiController');
    Route::apiResource('stadion-photo', 'StadionPhotoController');

    Route::get('booking-list', [\App\Http\Controllers\API\v1\RentApiController::class, 'bookingList'])->name('booking.list');
    Route::get('booking-list-player', [\App\Http\Controllers\API\v1\RentApiController::class, 'bookingListByPlayer'])->name('booking.list.player');
    Route::put('rent-payment-confirmation', [\App\Http\Controllers\API\v1\RentApiController::class, 'paymentConfirmation'])->name('booking.payment.confirmation');



    Route::get('/rent-all-info/price-list', [\App\Http\Controllers\API\v1\RentApiController::class, 'getAllInfo']);

    Route::get('/stadion-comment/list/{stadion_id}', [\App\Http\Controllers\API\v1\StadionApiController::class, 'showComments']);
    Route::get('/self-stadions/list', [\App\Http\Controllers\API\v1\StadionApiController::class, 'selfStadions']);
    Route::post('/stadion-comment/users', [\App\Http\Controllers\API\v1\StadionApiController::class, 'commentByUsers']);
    Route::post('/stadion-comment/stadion-owner', [\App\Http\Controllers\API\v1\StadionApiController::class, 'commentByOwner']);
    Route::get('/stadion-filter/name', [\App\Http\Controllers\API\v1\StadionApiController::class, 'findByName'])->name('stadion.filter.name');
    Route::get('/stadion-filter/map', [\App\Http\Controllers\API\v1\StadionApiController::class, 'findByMap'])->name('stadion.filter.map');
    Route::get('/stadion-filter/params', [\App\Http\Controllers\API\v1\StadionApiController::class, 'findByParams'])->name('stadion.filter.params');
    Route::put('/stadion-unactived/{stadion_id}', [\App\Http\Controllers\API\v1\StadionApiController::class, 'stadionUnactive']);


    Route::get('/team-logotips', [\App\Http\Controllers\API\v1\TeamApiController::class, 'clubLogotips'])->name('team.logotips');
    Route::get('/team/filter-players', [\App\Http\Controllers\API\v1\PlayerApiController::class, 'searchMembership']);
    Route::post('/team/add-members', [\App\Http\Controllers\API\v1\TeamApiController::class, 'membershipRequest']);
    Route::post('/team/add-banner', [\App\Http\Controllers\API\v1\TeamApiController::class, 'addBanner']);

    Route::put('/user-info/update', [\App\Http\Controllers\API\v1\UserInfoController::class, 'updateInfo']);
    Route::get('/user-info/show', [\App\Http\Controllers\API\v1\UserInfoController::class, 'index']);

    Route::post('/booked-by-stadion-owner', [\App\Http\Controllers\API\v1\RentApiController::class, 'bookedByStadionOwner']);



});






