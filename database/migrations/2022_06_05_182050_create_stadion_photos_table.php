<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStadionPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stadion_photos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('stadion_id')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();

            $table->foreign('stadion_id')
                ->references('id')
                ->on('stadiums')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stadion_photos');
    }
}
