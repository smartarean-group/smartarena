<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStadiumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stadiums', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('stadion_owner_id');
            $table->string('name_uz')->comment('Maydon nomi');
            $table->jsonb('day_of')->comment('Dam olish kunlari');
            $table->string('open_time')->nullable()->comment('Ish vaqtlari (boshlanish)');
            $table->string('close_time')->nullable()->comment('Ish vaqtlari (tugash)');
            $table->string('address')->nullable()->comment('Manziil');
            $table->jsonb('geoposition')->nullable()->comment('Xaritada joylashuv');
            $table->string('type')->nullable()->comment('Maydon turi');
            $table->jsonb('payment_type')->nullable()->comment('Tulov turi');
            $table->string('area_rules', 500)->nullable()->comment('Maydon qoidalari');
            $table->timestamps();

            $table->foreign('stadion_owner_id')
                ->references('id')
                ->on('stadion_owners')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stadiums');
    }
}
