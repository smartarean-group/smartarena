<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnForiegnStadiumsStadionOwnerIdForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stadiums', function (Blueprint $table) {
            $table->dropForeign(['stadion_owner_id']);

            $table->foreign('stadion_owner_id')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stadiums', function (Blueprint $table) {

            $table->dropForeign(['stadion_owner_id']);

            $table->foreign('stadion_owner_id')
                ->references('id')
                ->on('stadion_owners')
                ->onDelete('SET NULL');
        });
    }
}
