<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropNameRuColumnToStadiumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stadiums', function (Blueprint $table) {
            $table->dropColumn('name_ru');
            $table->dropColumn('name_en');
            $table->renameColumn('name_uz', 'name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stadiums', function (Blueprint $table) {
            $table->renameColumn('name', 'name_uz');
            $table->string('name_ru')->nullable();
            $table->string('name_en')->nullable();

        });
    }
}
