<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('stadion_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->smallInteger('is_positive')->default(1);
            $table->text('comment');
            $table->double('rate', 1,1)->default(0);
            $table->bigInteger('view');
            $table->timestamps();

            $table->foreign('stadion_id')
                ->references('id')
                ->on('stadiums')
                ->onDelete('SET NULL');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
