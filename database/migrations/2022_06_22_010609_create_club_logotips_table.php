<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubLogotipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_logotips', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('club_type');
            $table->string('logotip');
            $table->string('name_uz');
            $table->string('name_oz');
            $table->string('name_ru');
            $table->string('name_en');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_logotips');
    }
}
