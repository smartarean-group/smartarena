<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToTeamMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team_memberships', function (Blueprint $table) {
            $table->smallInteger('status')->default(0)->comment('0-Jarayonda; 1-Qabul qilindi; 2-Rad etildi; 3-Jamoa chiqarib yubordi; 4-Jamoadan uzi chiqib ketdi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team_memberships', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
