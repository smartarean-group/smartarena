<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamMembershipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_memberships', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('team_id');
            $table->unsignedBigInteger('player_id');
            $table->tinyInteger('player_number');
            $table->smallInteger('active')->default(0);
            $table->timestamps();

            $table->unique(['team_id', 'player_id'], 'team_id_player_id_unique');

            $table->foreign('team_id')
                ->references('id')
                ->on('teams')
                ->onDelete('no action');

            $table->foreign('player_id')
                ->references('id')
                ->on('players')
                ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_memberships');
    }
}
