<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColunmStadiusmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stadiums', function (Blueprint $table) {
            $table->string('arena_type')->nullable()->comment('Maydon Turi');
            $table->smallInteger('wear_room')->default(0)->comment('Kiyinish xonasi');
            $table->smallInteger('toilet')->default(0)->comment('Hojatxona');
            $table->smallInteger('wash_room')->default(0)->comment('Yuvinish xonasi');
            $table->smallInteger('car_parking')->default(0)->comment('Auto turargoh');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stadiums', function (Blueprint $table) {
            $table->dropColumn('arena_type');
            $table->dropColumn('wear_room');
            $table->dropColumn('toilet');
            $table->dropColumn('wash_room');
            $table->dropColumn('car_parking');
        });
    }
}
