<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->nullable();
            $table->string('full_name', 120)->nullable();
            $table->bigInteger('phone');
            $table->string('password');
            $table->string('password_show')->nullable();
            $table->smallInteger('status')->default(0);
            $table->integer('sms_code')->nullable();
            $table->boolean('active')->default(true);
            $table->smallInteger('whois')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->unique(["phone", "whois"], 'phone_whois_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
