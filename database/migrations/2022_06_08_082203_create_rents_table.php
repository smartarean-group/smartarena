<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rents', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('stadion_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->integer('arena_type_id')->nullable();
            $table->smallInteger('week')->nullable();
            $table->integer('time_id')->nullable();
            $table->date('c_date')->nullable();
            $table->timestamps();

            $table->foreign('stadion_id')
                ->references('id')
                ->on('stadiums')
                ->onDelete('SET NULL');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');

            $table->foreign('arena_type_id')
                ->references('id')
                ->on('arena')
                ->onDelete('SET NULL');

            $table->foreign('time_id')
                ->references('id')
                ->on('l_times')
                ->onDelete('SET NULL');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rents');
    }
}
