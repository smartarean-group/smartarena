<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArenaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arena', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('stadion_id')->nullable();
            $table->string('name')->nullable()->comment('Maydon nomi');
            $table->string('size')->nullable()->comment('Hajmi');
            $table->smallInteger('payment_type')->comment('Tulov turi');
            $table->timestamps();

            $table->foreign('stadion_id')
                ->references('id')
                ->on('stadiums')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arena');
    }
}
