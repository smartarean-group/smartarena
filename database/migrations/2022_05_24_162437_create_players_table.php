<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('first_name', 60)->comment('Ismi');
            $table->string('last_name', 60)->comment('Familiyasi');
            $table->date('birth_date')->comment('Tugilgan kuni');
            $table->string('tg_nickname', 30)->comment('Telegram username');
            $table->string('avatar')->nullable()->comment('Rasmi');
            $table->string('latitude', 10);
            $table->string('longitude',10);
            $table->integer('region_id');
            $table->integer('city_id');
            $table->string('address')->nullable()->comment('Manzil');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
