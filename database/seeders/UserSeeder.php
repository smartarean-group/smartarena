<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('phone', 998974406075)->first();
        if(is_null($user)) {
            $user = new User();
            $user->username = "superadmin";
            $user->full_name = "Super Admin";
            $user->phone = 998974406075;
            $user->password = Hash::make('admin123');
            $user->password_show = 'admin123';
            $user->whois = 1;
            $user->save();
            $user->assignRole('superadmin');
        }
    }
}
