<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\FileTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UserInfoController extends Controller
{
    use FileTrait;

    public function index()
    {
        $user = auth('api')->user();
        $user_id = $user->id;
        $user_info = User::selectRaw("id, split_part(full_name, ' ', 1) first_name, split_part(full_name, ' ', 2) last_name, phone, birth_date, avatar, tg_nickname, is_phone, is_tgname")->whereId($user_id)->first();

        return response()->successJson($user_info);
    }

    public function updateInfo(Request $request)
    {
        $user = auth('api')->user();
        $user_id = $user->id;

        $messages = [
            'first_name.required' => [
                'uz' => 'Ism maydonini kiritish shart',
                'ru' => 'Поле имя обязательно',
                'en' => 'The first_name field is required'
            ],
            'last_name.required' => [
                'uz' => 'Familiya maydonini kiritish shart',
                'ru' => 'Поле Фамилия обязательно',
                'en' => 'The last_name field is required'
            ]

        ];
        $input = $request->all();
        $validator = Validator::make($input, [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'tg_nickname' => ['required']
        ], $messages);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

//        if (!$user->hasRole('user_stadion')) {
//            $data = [
//                'message' => [
//                    'uz' => 'Bu role ga ruxsat berilmagan',
//                    'ru' => 'Эта роль не разрешена',
//                    'en' => 'This role is not authorized'
//                ]
//            ];
//            return response()->json($data, Response::HTTP_FORBIDDEN);
//        }

        $userInfo = User::find($user_id);
        $userInfo->full_name = $input['first_name'].' '.$input['last_name'];
        $userInfo->tg_nickname = $input['tg_nickname'];
        $userInfo->is_phone = $input['is_phone'];
        $userInfo->is_tgname = $input['is_tgname'];
        if($input['avatar']) {
            $avatar = $this->storeManual($input['avatar'], 'users');
            $userInfo->avatar = $avatar;
        }
        $userInfo->save();

        $res = [
            'success'=> true,
            'data' => '',
            'message' => 'Successfully update user info'
        ];
        return response()->json($res, Response::HTTP_OK);

    }

}
