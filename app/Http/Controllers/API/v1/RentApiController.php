<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ArenaResource;
use App\Models\Arena;
use App\Models\Rent;
use App\Models\Stadion;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RentApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    public function store(Request $request)
    {
        $user_id = auth('api')->user()->id;
        $input = $request->all();
        $validator = Validator::make($input, [
            'stadion_id' => ['required'],
            'arena_types' => ['required'],
            'week' => ['required'],
            'c_date' => ['required']
        ]);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

        try {
            $orders = [];
            $summa = [];
            foreach ($input['arena_types'] as $arena_type) {
                foreach ($arena_type['d_times'] as $key => $d_time) {
                    $rent = Rent::where([
                        "stadion_id" => $input['stadion_id'],
                        "arena_type_id" => $arena_type['arena_id'],
                        "time_id" => $d_time['id'],
                        "c_date" => $input['c_date']
                    ])->first();
                    if ($rent) {
                        return response()->json("Payment has been made in advance", Response::HTTP_OK);
                    }

                    $_rent = Rent::create([
                        "user_id" => $user_id,
                        "stadion_id" => $input['stadion_id'],
                        "arena_type_id" => $arena_type['arena_id'],
                        "week" => $input['week'],
                        "full_name" => $input['full_name']??null,
                        "phone" => $input['phone']??null,
                        "time_id" => $d_time['id'],
                        "c_date" => $input['c_date']
                    ]);
                    $per_hour = DB::table('l_times')->where('id', $_rent->time_id)->select('per_hour')->first()->per_hour;
                    $_arena = Arena::whereId($_rent->arena_type_id)->first();
                    $orders[$key]['id'] = $_rent->id;
                    $orders[$key]['arena_id'] = $_arena->name.'('.$_arena->size.')';
                    $orders[$key]['c_time'] = $per_hour;
                    $orders[$key]['c_date'] = $_rent->c_date;
                    $orders[$key]['price'] = $_arena->price;
                    $summa[] = $_arena->price;
                }
            }

            $orders['summa'] = array_sum($summa);
            $res = [
                'success' => true,
                'data' => $orders,
                'message' => 'ok'
            ];
            return response()->json($res, Response::HTTP_OK);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

    }

    public function paymentConfirmation(Request $request)
    {
        $input = $request->all();
        $receipt = [];
        $stadion = Stadion::whereId($input['stadion_id'])->first();
//        dd($stadion->city->name_uz);
        foreach ($input['accepts'] as $key => $accept) {
            $rent = Rent::find($accept['booking_id']);
            $rent->payment_confirmation = true;
            $rent->save();

            $per_hour = DB::table('l_times')->where('id', $rent->time_id)->select('per_hour')->first()->per_hour;
            $_arena = Arena::whereId($rent->arena_type_id)->first();
            $receipt[$key]['id'] = $rent->id;
            $receipt[$key]['arena_id'] = $_arena->name.'('.$_arena->size.')';
            $receipt[$key]['c_time'] = $per_hour;
            $receipt[$key]['c_date'] = $rent->c_date;
            $receipt[$key]['price'] = $_arena->price;
            $summa[] = $_arena->price;
        }
        $address = '';
        $address .= $stadion->region->name_uz." ";
        $address .= $stadion->city->name_uz." ";
        $address .= $stadion->address;
        $receipt['summa'] = array_sum($summa);
        $receipt['address'] = $address;
        $res = [
            'success' => true,
            'data' => $receipt,
            'message' => 'ok'
        ];
        return response()->json($res, Response::HTTP_OK);
    }

//    public function getAllInfo(Request $request)
//    {
//
//        $input = $request->all();
//        $stadion_id = $input['stadion_id'];
//        $c_date = $input['c_date'] ?? date('Y-m-d');
//
//        $date = new \DateTime($c_date);
//        $at_the_day = $date->format("w");
//
//        $stadion = Stadion::whereId($stadion_id)->with('arenas')->first();
//        if ($stadion == null)
//            return response()->json('Stadion mavjud emas', Response::HTTP_NOT_FOUND);
//
//        $arenas = $stadion->arenas;
//        $days = json_decode($stadion->day_of, true);
//
//        if(array_key_exists($at_the_day, $days)) {
//            return response()->json("A non-working day", Response::HTTP_OK);
//        }
//
//        $res = [];
//        foreach ($arenas as $key => $arena) {
//
//            $l_times = $this->lTime($stadion_id, $arena, $c_date);
//            $res[] = $arena;
//            $res[$key]['times'] = $l_times;
//
//        }
//
//        return response()->json($res, Response::HTTP_OK);
//    }

//    protected function lTimeOld($stadion_id, $arena, $c_date)
//    {
//        $user_id = auth('api')->user()->id;
//
//        $rents = Rent::where([
//            'stadion_id' => $stadion_id,
//            'user_id' => $user_id,
//            'arena_type_id' => $arena->id,
//            'c_date' => $c_date,
//        ])->get()->toArray();
//
//        $price = $arena->price;
//        $is_busy = 0;
//        $l_times = DB::table('l_times')
//            ->selectRaw("id, per_hour, '{$price}' as price, '{$is_busy}' AS is_busy")
//            ->get();
//        if(count($rents) > 0) {
//            $arr = [];
//            foreach($rents as $rent) {
//                $arr[$rent['time_id']] = $rent;
//            }
//
//            $res_time = [];
//            foreach($l_times as $l_time) {
//                $res_time[] = [
//                    'id' => $l_time->id,
//                    'per_hour' => $l_time->per_hour,
//                    'price' => $l_time->price,
//                    'is_busy' => isset($arr[$l_time->id]['time_id']) ? 1 : 0
//                ];
//            }
//
//            $l_times = $res_time;
//        }
//
//        return $l_times;
//    }

    protected function lTime($stadion_id, $arena, $c_date)
    {
        $user_id = auth('api')->user()->id;

        $rents = Rent::where([
            'stadion_id' => $stadion_id,
            'arena_type_id' => $arena->id,
            'c_date' => $c_date,
        ])->get()->toArray();

        //$price = $arena->price;
        $is_busy = 0;
        $l_times = DB::table('l_times')
            ->selectRaw("
                                   id,
                                   per_hour,
                                    {$is_busy}                             as is_busy,
                                   (case
                                       when
                                           DATE_PART('day',
                                                     to_char(now(), '{$c_date}, ' || substring(per_hour, 1, 2) || ':MI:SS')::timestamp -
                                                     to_char(now(), 'YYYY-MM-DD HH24:MI:SS')::timestamp) * 24 +
                                           DATE_PART('hour',
                                                     to_char(now(), '{$c_date}, ' || substring(per_hour, 1, 2) || ':MI:SS')::timestamp -
                                                     to_char(now(), 'YYYY-MM-DD HH24:MI:SS')::timestamp) > 0 then 0
                                       else 1 end) as disabled")
            ->get();

        if (count($rents) > 0) {
            $arr = [];
            foreach ($rents as $rent) {
                $arr[$rent['time_id']] = $rent;
            }

            $res_time = [];
            foreach ($l_times as $l_time) {
                $user_id = "";
                $full_name = "";
                $phone = "";
                $tg_nickname = "";
                if (isset($arr[$l_time->id]['user_id'])) {
                    $user = User::whereId($arr[$l_time->id]['user_id'])->selectRaw('full_name, phone, tg_nickname')->first();
                    $user_id = $user->id;
                    $full_name = $user->full_name;
                    $phone = $user->phone;
                    $tg_nickname = $user->tg_nickname;
                }
                $res_time[] = [
                    'id' => $l_time->id,
                    'user_id' => $user_id,
                    'full_name' => $arr[$l_time->id]['full_name'] ?? $full_name,
                    'phone' => $arr[$l_time->id]['phone'] ?? $phone,
                    'tg_nickname' => $tg_nickname,
                    'per_hour' => $l_time->per_hour,
                    //'price' => $l_time->price,
                    'is_busy' => isset($arr[$l_time->id]['time_id']) ? 1 : 0,
                    'disabled' => $l_time->disabled
                ];
            }

            $l_times = $res_time;
        }

        return [
            'times' => $l_times,
            'message' => 'Working time'
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rent = Rent::find($id);
        $rent->delete();
        $data = [
            'message' => 'Muvafiqqiyatli o\'chirildi!'
        ];
        return response()->json($data);
    }

    public function bookedByStadionOwner(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'stadion_id' => ['required'],
            'arena_types' => ['required'],
            'week' => ['required'],
            'c_date' => ['required']
        ]);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

        try {
            foreach ($input['arena_types'] as $arena_type) {
                foreach ($arena_type['d_times'] as $d_time) {
                    $rent = Rent::where([
                        "stadion_id" => $input['stadion_id'],
                        "arena_type_id" => $arena_type['arena_id'],
                        "time_id" => $d_time['id'],
                        "c_date" => $input['c_date']
                    ])->first();
                    if ($rent) {
                        return response()->json("Payment has been made in advance", Response::HTTP_OK);
                    }
                    Rent::create([
                        "stadion_id" => $input['stadion_id'],
                        "arena_type_id" => $arena_type['arena_id'],
                        "week" => $input['week'],
                        "time_id" => $d_time['id'],
                        "c_date" => $input['c_date'],
                        "full_name" => $input['full_name'] ?? null,
                        "phone" => $input['phone'] ?? null
                    ]);
                }
            }
            $res = [
                'success' => true,
                'data' => '',
                'message' => 'ok'
            ];
            return response()->json($res, Response::HTTP_OK);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function bookingList(Request $request)
    {
        $input = $request->all();
        $stadion_id = $input['stadion_id'];
        $arena_id = $input['arena_id'];
        $c_date = $input['c_date'] ?? date('Y-m-d');

        $date = new \DateTime($c_date);
        $at_the_day = $date->format("w");

        $arena = Arena::find($arena_id);

        $stadion = Stadion::whereId($stadion_id)->with('arenas')->first();

        if ($stadion == null)
            return response()->json('Stadion mavjud emas', Response::HTTP_NOT_FOUND);

        $arenas = new ArenaResource($stadion->arenas);

        $days = json_decode($stadion->day_of, true);

        $l_times = $this->lTime($stadion_id, $arena, $c_date);
        if (array_key_exists($at_the_day, $days)) {
            $l_times = [
                'times' => [],
                'message' => 'A non-working day'
            ];
        }

        $res['select']['id'] = $arena->id;
        $res['select']['name'] = $arena->name;
        $res['select']['size'] = $arena->size;
        $res['select']['price'] = $arena->price;
        $res['select']['booking_list'] = $l_times;
        $res['arena_tabs'] = $arenas;


        return response()->json($res, Response::HTTP_OK);

    }

    public function bookingListByPlayer(Request $request)
    {
        $input = $request->all();
        $stadion_id = $input['stadion_id'];
        $arena_id = $input['arena_id'];
        $c_date = $input['c_date'] ?? date('Y-m-d');

        $date = new \DateTime($c_date);
        $at_the_day = $date->format("w");

        $arena = Arena::find($arena_id);

        $stadion = Stadion::whereId($stadion_id)->with('arenas')->first();
        if ($stadion == null)
            return response()->json('Stadion mavjud emas', Response::HTTP_NOT_FOUND);

        $arenas = new ArenaResource($stadion->arenas);

        $days = json_decode($stadion->day_of, true);

        $l_times = $this->lTime($stadion_id, $arena, $c_date);
        if (array_key_exists($at_the_day, $days)) {
            $l_times = [
                'times' => [],
                'message' => 'A non-working day'
            ];
        }

        $res['select']['id'] = $arena->id;
        $res['select']['name'] = $arena->name;
        $res['select']['size'] = $arena->size;
        $res['select']['price'] = $arena->price;
        $res['select']['booking_list'] = $l_times;
        $res['arena_tabs'] = $arenas;


        return response()->json($res, Response::HTTP_OK);

    }
}
