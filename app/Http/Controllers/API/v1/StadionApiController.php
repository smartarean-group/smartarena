<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Filter\StadiumsFilter;
use App\Http\Requests\StadionRequest;
use App\Http\Resources\StadionResource;
use App\Models\Arena;
use App\Models\ReplyToComment;
use App\Models\Stadion;
use App\Models\StadionComment;
use App\Models\StadionPhoto;
use App\Traits\FileTrait;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;


class StadionApiController extends Controller
{
    use FileTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stadions = new StadiumsFilter($request);
        $stadions = $stadions->apiFilter();

        $stadions = $stadions->with('stadionPhotos')
            ->orderBy('id', 'desc')
            ->paginate(10);
        return response()->successJson($stadions);
    }

    public function selfStadions(Request $request)
    {
        $user = auth('api')->user();
        $user_id = $user->id;
        $stadions = new StadiumsFilter($request);
        $stadions = $stadions->apiFilter();

        $stadions = $stadions->with('stadionPhotos')
            ->where('stadion_owner_id', $user_id)
            ->orderBy('id', 'desc')
            ->paginate(10);
        return response()->successJson($stadions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StadionRequest $request)
    {
        $user = auth('api')->user();
        $user_id = $user->id;

        $input = $request->all();
        Log::info($input);
        try {
            $stadion = Stadion::create([
                "stadion_owner_id" => $user_id,
                "name" => $input['name'],
                "day_of" => json_encode($input['day_of']),
                "open_time" => $input['open_time'],
                "close_time" => $input['close_time'],
                "region_id" => $input['region_id'],
                "city_id" => $input['city_id'],
                "address" => $input['address'],
                "wear_room" => $input['wear_room'],
                "payment_cash" => $input['payment_cash'],
                "payment_card" => $input['payment_card'],
                "toilet" => $input['toilet'],
                "wash_room" => $input['wash_room'],
                "car_parking" => $input['car_parking'],
                "sunshade" => $input['sunshade'],
                "lighter" => $input['lighter'],
                "geoposition" => json_encode($input['geoposition']),
                "arena_type" => $input['arena_type'],
                "area_rules" => $input['area_rules']
            ]);

            if ($input['arenas']) {
                try {
                    foreach ($input['arenas'] as $arena) {
                        Arena::create([
                            "stadion_id" => $stadion->id,
                            "name" => $arena['name'],
                            "size" => $arena['size'],
                            "price" => $arena['price']
                        ]);
                    }

                } catch (\Exception $e) {
                    Stadion::whereId($stadion->id)->delete();
                    Log::error($e->getMessage());
                    return response()->json($e->getMessage(), Response::HTTP_BAD_REQUEST);
                }
            }

            if ($input['photos']) {
                try {
                    foreach ($input['photos'] as $photo) {
                        StadionPhoto::create([
                            "stadion_id" => $stadion->id,
                            "image" => $this->storeManual($photo['s_photo'], 'stadiums')
                        ]);
                    }
                } catch (\Exception $e) {
                    $arenas = Arena::where('stadion_id', $stadion->id)->get();
                    Log::error($arenas);
                    foreach ($arenas as $arena) {
                        $arena->delete();
                    }
                    Stadion::whereId($stadion->id)->delete();
                    return response()->json($e->getMessage(), Response::HTTP_BAD_REQUEST);
                }
            }
            $res = [
                'success' => true,
                'data' => $stadion,
                'message' => 'ok'
            ];
            return response()->json($res, Response::HTTP_OK);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json($exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return array
     */
    public function show(Stadion $stadion)
    {
        return [
            'success' => true,
            'data' => new StadionResource($stadion),
            'message' => 'Stadion show'
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(StadionRequest $request, $id)
    {
        $user = auth('api')->user();
        $user_id = $user->id;

        $input = $request->all();
        Log::info($input);
        try {
            $stadion = Stadion::find($id);
            $stadion->stadion_owner_id = $user_id;
            $stadion->name             = $input['name'];
            $stadion->day_of           = json_encode($input['day_of']);
            $stadion->open_time        = $input['open_time'];
            $stadion->close_time       = $input['close_time'];
            $stadion->region_id        = $input['region_id'];
            $stadion->city_id          = $input['city_id'];
            $stadion->address          = $input['address'];
            $stadion->payment_cash     = $input['payment_cash'];
            $stadion->payment_card     = $input['payment_card'];
            $stadion->wear_room        = $input['wear_room'];
            $stadion->toilet           = $input['toilet'];
            $stadion->wash_room        = $input['wash_room'];
            $stadion->car_parking      = $input['car_parking'];
            $stadion->sunshade         = $input['sunshade'];
            $stadion->lighter          = $input['lighter'];
            $stadion->geoposition      = json_encode($input['geoposition']);
            $stadion->arena_type       = $input['arena_type'];
            $stadion->area_rules       = $input['area_rules'];

            $stadion->save();

            $res = [
                'success' => true,
                'data' => $stadion,
                'message' => 'ok'
            ];

            return response()->json($res, Response::HTTP_OK);
        } catch (QueryException $exception) {
            Log::error($exception->errorInfo);
            return response()->errorJson($exception->errorInfo, Response::HTTP_BAD_REQUEST);
        }


//        $stphotos = StadionPhoto::where('stadion_id', $stadion->id)->get();
//        foreach ($stphotos as $stphoto) {
//                $path_file = str_replace(env('APP_URL').'/storage', '', $stphoto->image);
//                Storage::delete($path_file);
//        }
//
//        if ($input['photos']) {
//            StadionPhoto::where('stadion_id', $stadion->id)->delete();
//
//            foreach ($input['photos'] as $photo) {
//                StadionPhoto::create([
//                    "stadion_id" => $stadion->id,
//                    "image" => $this->storeManual($photo['s_photo'], 'stadiums')
//                ]);
//            }
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function stadionUnactive($stadion_id)
    {
        $user = auth('api')->user();
        $user_id = $user->id;

        $stadion = Stadion::whereId($stadion_id)->where('stadion_owner_id', $user_id)->first();
        if ($stadion->active) {
            $stadion->update(['active' => false]);
            $mess = "Stadium not activation";
        } else {
            $stadion->update(['active' => true]);
            $mess = "Stadium activation";
        }

        $res = [
            'success' => true,
            'data' => $stadion,
            'message' => $mess
        ];
        return response()->json($res, Response::HTTP_OK);
    }

    /**
     * Foydalanuvchilar tomonidan yozilgan kommentlar
     **/
    public function commentByUsers(Request $request)
    {
        $user = auth('api')->user();
        $user_id = $user->id;
        $input = $request->all();
        $validator = Validator::make($input, [
            'stadion_id' => ['required'],
            'comment' => ['required'],
            'rating' => ['required'],
        ]);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }
        if (!$user->hasRole('user_player')) {
            $data = [
                'message' => [
                    'uz' => 'Bu role ga ruxsat berilmagan',
                    'ru' => 'Эта роль не разрешена',
                    'en' => 'This role is not authorized'
                ]
            ];
            return response()->json($data, Response::HTTP_FORBIDDEN);
        }

        try {
            $data = StadionComment::create([
                "stadion_id" => $input["stadion_id"],
                "user_id" => $user_id,
                "comment" => $input["comment"],
                "rating" => $input["rating"]
            ]);
            $res = [
                'success' => true,
                'data' => $data,
                'message' => 'Successfully rated'
            ];
            return response()->json($res, Response::HTTP_OK);
        } catch (\Exception $e) {
            return \response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Stadion egasi tomonidan yozilgan kommentlar*/
    public function commentByOwner(Request $request)
    {
        $user = auth('api')->user();
        $user_id = $user->id;
        $input = $request->all();
        $validator = Validator::make($input, [
            'stadion_comment_id' => ['required'],
            'comment' => ['required']
        ]);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

//        if (!$user->hasRole('user_stadion')) {
//            $data = [
//                'message' => [
//                    'uz' => 'Bu role ga ruxsat berilmagan',
//                    'ru' => 'Эта роль не разрешена',
//                    'en' => 'This role is not authorized'
//                ]
//            ];
//            return response()->json($data, Response::HTTP_FORBIDDEN);
//        }

        try {
            $data = ReplyToComment::create([
                'stadion_comment_id' => $input['stadion_comment_id'],
                'stadion_owner_id' => $user_id,
                'comment' => $input['comment']
            ]);
            $res = [
                'success' => true,
                'data' => $data,
                'message' => 'Successfully rated'
            ];
            return response()->json($res, Response::HTTP_OK);
        } catch (\Exception $exception) {
            $responseArr = [];
            $responseArr['success'] = false;
            $responseArr['message'] = $exception->getMessage();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

    }

    public function showComments($stadion_id)
    {
        $std_comments = StadionComment::where('stadion_id', $stadion_id)->with('replyComment')->get();

        return response()->json($std_comments, Response::HTTP_OK);
    }

    public function findByName(Request $request)
    {
        $input = $request->all();
        $messages = [
            'name.required' => [
                'uz' => 'name maydonini kiritish shart',
                'ru' => 'Поле name обязательно',
                'en' => 'The name field is required'
            ]
        ];
        $validator = Validator::make($input, [
            'name' => ['required']
        ], $messages);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

        $stadions = new StadiumsFilter($request);
        $stadions = $stadions->apiFilter();

        $stadions = $stadions->paginate(10);
        $res = [
            'success' => true,
            'data' => $stadions,
            'message' => 'ok'
        ];
        return response()->json($res, Response::HTTP_OK);
    }

    public function findByParams(Request $request)
    {
        $input = $request->all();
        $messages = [
            'region_id.required' => [
                'uz' => 'region_id maydonini kiritish shart',
                'ru' => 'Поле region_id обязательно',
                'en' => 'The region_id field is required'
            ],
            'city_id.required' => [
                'uz' => 'city_id maydonini kiritish shart',
                'ru' => 'Поле city_id обязательно',
                'en' => 'The city_id field is required'
            ]
        ];
        $validator = Validator::make($input, [
            'region_id' => ['required'],
            'city_id' => ['required']
        ], $messages);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }
        $stadions = new StadiumsFilter($request);
        $stadions = $stadions->apiFilter();

        $stadions = $stadions->with('stadionPhotos');

//        $stadions = $stadions->orderBy('id', 'desc');

        $stadions = $stadions->paginate(10);
        $res = [
            'success' => true,
            'data' => $stadions,
            'message' => 'ok'
        ];
        return response()->json($res, Response::HTTP_OK);
    }

    public function findByMap(Request $request)
    {
        $messages = [
            'latitude.required' => [
                'uz' => 'Kenglik maydonini kiritish shart',
                'ru' => 'Поле широты обязательно',
                'en' => 'The latitude field is required'
            ],
            'longitude.required' => [
                'uz' => 'Uzunlik maydonini kiritish shart',
                'ru' => 'Поле долготы обязательно',
                'en' => 'The longitude field is required'
            ],
            'distance.required' => [
                'uz' => 'Masofa maydoni talab qilinadi',
                'ru' => 'Поле расстояния обязательно',
                'en' => 'The distance field is required'
            ],
            'is_whois.required' => [
                'uz' => 'is_whois maydoni talab qilinadi.',
                'ru' => 'Поле whois является обязательным.',
                'en' => 'The is whois field is required.'
            ],
            'arena_type.required' => [
                'uz' => 'Arena turi maydoni talab qilinadi',
                'ru' => 'Поле типа арены обязательно',
                'en' => 'The arena type field is required'
            ],

        ];
        $input = $request->all();
        $validator = Validator::make($input, [
            'latitude' => ['required'],
            'longitude' => ['required'],
            'distance' => ['required'],
            'is_whois' => ['required'],
            'arena_type' => ['required', 'array'],
        ], $messages);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

        $distance_km = $input['distance'];
        $latitude = $input['latitude'];
        $longitude = $input['longitude'];
        $is_whois = $input['is_whois'];
        $arena_types = implode('|', $input['arena_type']);

        if ($is_whois == 'stadion') {
            $sql = "select *
                    from (
                        select
                                st.id,
                                st.name,
                                st.address,
                                geoposition::json->>'lat' as latitude,
                                geoposition::json->>'long' as longitude,
                                (((acos(sin(($latitude * pi() / 180)) *
                                         sin(((geoposition::json->>'lat')::double precision * pi() / 180)) +
                                         cos(($latitude * pi() / 180)) *
                                         cos(((geoposition::json->>'lat')::double precision * pi() / 180)) *
                                         cos((($longitude - (geoposition::json->>'long')::double precision) * pi() / 180)))) *
                                   180 / pi()) * 60 * 1.1515 *
                                  1.609344)                                     as distance,
                                st.arena_type as arena_type
                          from stadiums as st
                          WHERE arena_type ~* '$arena_types') as markers
                    WHERE distance <= $distance_km

                    order by distance asc";
        } elseif ($is_whois == 'player') {
            $sql = "select *
                    from (
                             select
                                p.id,
                                p.first_name,
                                p.last_name,
                                p.latitude as latitude,
                                p.longitude as longitude,
                                (((acos(sin(($latitude * pi() / 180)) *
                                         sin(((p.latitude)::double precision * pi() / 180)) +
                                         cos(($latitude * pi() / 180)) *
                                         cos(((p.latitude)::double precision * pi() / 180)) *
                                         cos((($longitude - (p.longitude)::double precision) * pi() / 180)))) *
                                   180 / pi()) * 60 * 1.1515 *
                                  1.609344)                                     as distance
                          from palyers as st) as markers
                    WHERE distance <= $distance_km
                    order by distance asc";
        }
        $data_map = DB::select($sql);
        return response()->json($data_map);

    }
}
