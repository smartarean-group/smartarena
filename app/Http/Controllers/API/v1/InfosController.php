<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Region;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class InfosController extends Controller
{
    public function __construct()
    {
        $this->middleware('custom_auth', ['except' => ['regionList', 'cityListByRegion']]);
    }
    public function regionList()
    {
        $regions = Region::all();
        return response()->json($regions, Response::HTTP_OK);
    }

    public function cityListByRegion($region_id)
    {
        $messages = [
            'region_id.required' => [
                'uz'=> 'Maydonnni kiritish shart',
                'ru'=> 'Поле обязательно',
                'en'=> 'Field is required'
            ],
            'region_id.numeric' => [
                'uz'=> 'Region_id raqam bo\'lishi kerak.',
                'ru'=> 'Region_id должен быть числом.',
                'en'=> 'The region id must be a number.'
            ]
        ];
        $validator = Validator::make(['region_id' => $region_id], [
            'region_id' => 'required|numeric'
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 422);
        }

        $city_by_region = City::where('region_id', $region_id)->get();

        return response()->json($city_by_region, Response::HTTP_OK);
    }


}
