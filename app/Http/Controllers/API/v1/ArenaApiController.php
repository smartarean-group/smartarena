<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArenaRequest;
use App\Models\Arena;
use App\Models\Rent;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class ArenaApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ArenaRequest $request)
    {
        $input = $request->all();
        Log::info($input);

        try {
            $arena = Arena::create([
                "stadion_id" => $input['stadion_id'],
                "name"       => $input['name'],
                "size"       => $input['size'],
                "price"      => $input['price']
            ]);
            return response()->json($arena, Response::HTTP_OK);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json($exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ArenaRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $arena = Arena::find($id);

        if(Rent::where('stadion_id', $arena->stadion_id)->where('arena_type_id', $id)->exists()) {
            $rent = Rent::where('stadion_id', $arena->stadion_id)
                ->where('arena_type_id', $id)
                ->where('c_date', '!=', now()->toDateString())
                ->first();

            if ($rent != null) {
                $arena->current_price = false;
                $arena->save();
                try {
                    $_arena = Arena::create([
                        "stadion_id" => $arena->stadion_id,
                        "name"       => $input['name'],
                        "size"       => $input['size'],
                        "price"      => $input['price']
                    ]);
                    return response()->json($_arena, Response::HTTP_OK);
                } catch (\Exception $exception) {
                    Log::error($exception->getMessage());
                    return response()->json($exception->getMessage(), Response::HTTP_BAD_REQUEST);
                }
            }
            $data = [
                'message' => 'Ushbu maydonni o\'zgartira olmaysiz! Chunki joriy narx bilan buyrutma qilingan. Ertaga o\'rinib ko\'rishingiz mumkin.'
            ];
            return response()->json($data);
        }

        $arena->name  = $input['name'];
        $arena->size  = $input['size'];
        $arena->price = $input['price'];
        $arena->save();
        return response()->successJson($arena);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arena = Arena::find($id);

        if(Rent::where('stadion_id', $arena->stadion_id)->where('arena_type_id', $id)->exists()) {
            $rent = Rent::where('stadion_id', $arena->stadion_id)
                ->where('arena_type_id', $id)
                ->where('c_date', now()->toDateString())
                ->first();
            if ($rent != null) {
                $arena->current_price = false;
                $arena->save();
                $data = [
                    'message' => 'Muvafiqqiyatli o\'chirildi!'
                ];
                return response()->json($data);
            }
            $data = [
                'message' => 'Ushbu maydonni o\'chira olmaysiz! Chunki maydon joriy narx bilan buyrutma qilingan. Ertaga o\'rinib ko\'rishingiz mumkin.'
            ];
            return response()->json($data);
        }
        $arena->delete();
        $data = [
            'message' => 'Muvafiqqiyatli o\'chirildi!'
        ];
        return response()->json($data);
    }
}
