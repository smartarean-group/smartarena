<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Models\Player;
use App\Traits\FileTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class PlayerApiController extends Controller
{
    use FileTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = auth('api')->user()->id;

        $messages = [
            'latitude.required' => [
                'uz' => 'Kenglik maydonini kiritish shart',
                'ru' => 'Поле широта обязательно',
                'en' => 'The latitude field is required'
            ],
            'longitude.required' => [
                'uz' => 'Uzunlik maydonini kiritish shart',
                'ru' => 'Поле долготы обязательно',
                'en' => 'The longitude field is required'
            ],
            'region_id.required' => [
                'uz' => 'Region_id maydonini kiritish shart',
                'ru' => 'Поле region_id обязательно',
                'en' => 'The region_id field is required'
            ],
            'city_id.required' => [
                'uz' => 'City_id maydonini kiritish shart',
                'ru' => 'Поле city_id обязательно',
                'en' => 'The city_id field is required'
            ],
        ];
        $validator = Validator::make($request->all(), [
            'latitude' => ['required'],
            'longitude' => ['required'],
            'region_id' => ['required'],
            'city_id' => ['required']
        ], $messages);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }
        $input = $request->all();

        try {
            $player = Player::create([
                'user_id' => $user_id,
                'latitude' => $input['latitude'],
                'longitude' => $input['longitude'],
                'region_id' => $input['region_id'],
                'city_id' => $input['city_id'],
                'address' => $input['address']??null
            ]);
            $data = [
                'success' => true,
                'message' => "Successfully save",
                'errors' => '',
                'data' => $player
            ];
            return response()->json($data, Response::HTTP_OK);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchMembership(Request $request)
    {
        $input = $request->all();
        dd($input);
        $validator = Validator::make($input, [
            'region_id' => ['required'],
            'city_id' => ['required'],
        ]);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }
        $region_id = $input['region_id'];
        $city_id = $input['city_id'];
        $players = Player::where('region_id', $region_id)
            ->where('city_id', $city_id)
            ->whereNull('team_membership')
            ->get();
        $data = [
            'success' => true,
            'message' => "Successfully save",
            'errors' => '',
            'data' => $players
        ];
        return response()->json($data, Response::HTTP_OK);
    }
}
