<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\TeamBanner;
use App\Models\TeamMembership;
use App\Traits\FileTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TeamApiController extends Controller
{
    use FileTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = auth('api')->user()->id;
        $input = $request->all();
        $validator = Validator::make($input, [
            'name_uz' => ['required', 'string'],
            'club_logotip_id' => ['required']
        ]);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

        try {
            $data = Team::create([
                'user_id' => $user_id,
                'name_uz' => $input['name_uz'],
                'club_logotip_id' => $input['club_logotip_id'],
                'description' => $input['description']??null
            ]);
            $res = [
                'success'=> true,
                'data' => $data,
                'message' => 'Successfully save'
            ];
            return response()->json($res, Response::HTTP_OK);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function clubLogotips()
    {
       $datas =  DB::table('club_logotips');
       $datas = $datas->selectRaw("
                   club_type,
                   CASE
                       when club_type = 1 then 'International teams'
                       when club_type = 2 then 'National clubs'
                       when club_type = 3 then 'Top clubs' end as club_name,
                   count('club_type')                          as count
       ");
       $datas = $datas->groupBy('club_type');
       $datas = $datas->orderBy('club_type', 'asc');
       $datas = $datas->get();
       $res = [];
       foreach($datas as $data) {
           $logotips =  DB::table('club_logotips')->selectRaw('id, logotip, name_uz, name_oz, name_ru, name_en')->where('club_type', $data->club_type)->get();
           $data->logotips = $logotips;
            $res[]  = $data;
        }

        return response()->json($res, Response::HTTP_OK);
    }



    public function membershipRequest(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'team_id' => ['required'],
            'players' => ['required', 'array']
        ]);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

        $players = $input['players'];
        try {
            foreach ($players as $player) {

                TeamMembership::create([
                    'team_id' => $input['team_id'],
                    'player_id' => $player['id']
                ]);
            }
            $res = [
                'success'=> true,
                'data' => '',
                'message' => 'Successfully save'
            ];
            return response()->json($res, Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage());
        }
    }

    public function addBanner(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'team_id' => ['required']
        ]);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

        try {
            $data = TeamBanner::create([
               'team_id' => $input['team_id'],
               'image' => $this->storeManual($input['image'], 'teams')??null
            ]);
            $res = [
                'success'=> true,
                'data' => $data,
                'message' => 'Successfully save'
            ];
            return response()->json($res, Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage());
        }


    }
}
