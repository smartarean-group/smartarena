<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SmsController;
use App\Models\Player;
use App\Models\User;
use App\Traits\FileTrait;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;

class AuthController extends Controller
{
    use FileTrait;

    protected $sms;
    public function __construct()
    {
        $this->middleware('custom_auth', ['except' => ['login', 'register', 'confirm', 'logout', 'reset-pass', 'resend', 'resetPassword', 'addUser']]);
        $this->sms = new SmsController();
    }

    /**
     * Register user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $messages = [
            'phone.required' => [
                'uz'=> 'Telefon maydonini kiritish shart',
                'ru'=> 'Поле телефон обязательно',
                'en'=> 'The phone field is required'
            ],
            'phone.digits_between' => [
                'uz' => 'Telefon 12 dan 12 gacha raqamdan iborat bo\'lishi kerak',
                'ru' => 'Телефон должен содержать от 12 до 12 цифр',
                'en' => 'The phone must be between 12 and 12 digits'
            ],
            'whois.required' => [
                'uz' => 'Whois maydonini kiritish shart',
                'ru' => 'Поле whois обязательно',
                'en' => 'The whois field is required'
            ],
            'first_name.required' => [
                'uz' => 'Ism maydonini kiritish shart',
                'ru' => 'Поле имя обязательно',
                'en' => 'The first_name field is required'
            ],
            'last_name.required' => [
                'uz' => 'Familiya maydonini kiritish shart',
                'ru' => 'Поле Фамилия обязательно',
                'en' => 'The last_name field is required'
            ],
            'birth_date.required' => [
                'uz' => 'Tug‘ilgan sana maydonini kiritish shart',
                'ru' => 'Поле Дата рождения обязательно',
                'en' => 'The Date of birth field is required'
            ],
            'birth_date.date_format' => [
                'uz' => '1999-01-01 ushbu formatda kiritish shart.',
                'ru' => '1999-01-01 необходимо вводить в этом формате.',
                'en' => '1999-01-01 must be entered in this format.'
            ],
            'region_id.required' => [
                'uz' => 'Viloyatni tanlang.',
                'ru' => 'Выберите регион',
                'en' => 'Select a region'
            ],
            'city_id.required' => [
                'uz' => 'Tumanni tanlang.',
                'ru' => 'Выберите район',
                'en' => 'Select a district'
            ],
            'tg_nickname.required' => [
                'uz' => 'Telegram niknomi maydonini kiritish shart',
                'ru' => 'Поле никнейма Telegram должно быть заполнено',
                'en' => 'Telegram nickname field must be entered'
            ],
        ];
        $input = $request->all();

        $validator = Validator::make($input, [
            'phone' => ['required', 'numeric', 'digits_between:12,12'],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'whois' => ['required']
        ], $messages);
        $phone = $input['phone'];
        $whois = $input['whois'];

        if ($phone == '998004444444') {
            //For Test
            $responseArr['message'] = 'Ushbu telefon raqam bazada mavjud';
            return response()->json($responseArr, Response::HTTP_NOT_ACCEPTABLE);
        }


        $user = User::where('phone', $phone)->where('whois', $whois)->where('status', 1)->first();

        if($user) {
            //Bazada mavjud
            $responseArr['message'] = 'Ushbu telefon raqam bazada mavjud';
            return response()->json($responseArr, Response::HTTP_NOT_ACCEPTABLE);
        }

        if ($whois == 3) { //Futbol uyinchisi
            $role = 'user_player';
            $validator = Validator::make($input, [
                'birth_date' => ['required', 'date_format:Y-m-d'],
                'region_id' => ['required'],
                'city_id' => ['required'],
                'tg_nickname' => ['required'],
            ], $messages);

        } elseif ($whois == 4) { //Stadion rahbari
            $role = 'user_stadion';
        } else {
            $data = [
                'message'=> [
                    'role' => 'Role not included'
                ]
            ];
            return response()->json($data, Response::HTTP_NOT_ACCEPTABLE);
        }

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

        $avatar = null;
        if (isset($input['avatar']) && $input['avatar']) {
            $avatar = $this->storeManual($input['avatar'], 'users');
        }

        try {

            $user = User::updateOrCreate(
                [
                    'phone' => $phone,
                    'whois' => $whois,
                ],
                [
                    'phone' => $phone,
                    'password' => Hash::make($phone),
                    'whois' => $whois,
                    'full_name' => $input['first_name'].' '.$input['last_name'],
                    'birth_date' => $input['birth_date']??null,
                    'tg_nickname' => $input['tg_nickname']??null,
                    'avatar' => $avatar
                ]
            );
            $user->assignRole($role);

            return $this->sms->getCode($phone, $whois);

        } catch (QueryException $exception) {
            $data = [
                'success' => false,
                'errors' => $exception->getMessage(),
                'data' => ''
            ];
            return  response()->json($data, Response::HTTP_NOT_ACCEPTABLE);
        }

    }
    /**
     * Register user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
//    public function register(Request $request)
//    {
//        $messages = [
//            'phone.required' => [
//                'uz'=> 'Telefon maydonini kiritish shart',
//                'ru'=> 'Поле телефон обязательно',
//                'en'=> 'The phone field is required'
//            ],
//            'phone.digits_between' => [
//                'uz' => 'Telefon 12 dan 12 gacha raqamdan iborat bo\'lishi kerak',
//                'ru' => 'Телефон должен содержать от 12 до 12 цифр',
//                'en' => 'The phone must be between 12 and 12 digits'
//            ],
//            'whois.required' => [
//                'uz' => 'Whois maydonini kiritish shart',
//                'ru' => 'Поле whois обязательно',
//                'en' => 'The whois field is required'
//            ]
//        ];
//        $input = $request->all();
//        $validator = Validator::make($input, [
//            'phone' => ['required', 'numeric', 'digits_between:12,12'],
//            'whois' => ['required']
//        ], $messages);
//
//        if ($validator->fails()) {
//            $responseArr = [];
//            $responseArr['message'] = $validator->errors();
//            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
//        }
//
//        $phone = $input['phone'];
//        $whois = $input['whois'];
//        $user = User::where('phone', $phone)->where('whois', $whois)->where('status', true)->first();
//
//        if($user) {
//            return $this->userLogin($user);
//        }
//
//        try {
//            if ($whois == 3) { //Futbol uyinchisi
//                $role = 'user_player';
//
//            } elseif ($whois == 4) { //Stadion rahbari
//                $role = 'user_stadion';
//            } else {
//                $data = [
//                    'message'=> [
//                        'role' => 'Role not included'
//                    ]
//                ];
//                return response()->json($data, Response::HTTP_BAD_REQUEST);
//            }
//
//            $validator = Validator::make($input, [
//                'first_name' => ['required'],
//                'last_name' => ['required']
//            ]);
//
//            if ($validator->fails()) {
//                $responseArr = [];
//                $responseArr['message'] = $validator->errors();
//                return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
//            }
//
//            $user = User::updateOrCreate(
//                [
//                    'phone' => $phone,
//                    'whois' => $whois,
//                ],
//                [
//                    'phone' => $phone,
//                    'password' => Hash::make($phone),
//                    'whois' => $whois,
//                    'full_name' => $input['first_name'].' '.$input['last_name'],
//                    'birth_date' => $input['birth_date']??null,
//                    'tg_nickname' => $input['tg_nickname']??null,
//                    'avatar' => $this->storeManual($input['avatar'], 'users')??null
//                ]
//            );
//            $user->assignRole($role);
//
//            return $this->sms->getCode($phone, $whois);
//
//        } catch (QueryException $exception) {
//            $data = [
//                'success' => false,
//                'errors' => $exception->getMessage(),
//                'data' => 'Ushbu telefon raqam oldindan royhatdan otgan!!!'
//            ];
//            return  response()->json($data, Response::HTTP_NOT_ACCEPTABLE);
//        }
//
//    }


//    public function register(Request $request)
//    {
//        $input = $request->all();
//
//        $validator = Validator::make($input, [
//            'phone' => ['required', 'numeric', 'digits_between:12,12'],
//            'password' => ['required', 'string', 'min:6', 'confirmed'],
//            'whois' => ['required']
//        ]);
//
//        if ($validator->fails()) {
//            $responseArr = [];
//            $responseArr['message'] = $validator->errors();
//            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
//        }
//
//        $user = User::where('phone', $input['phone'])->where('status', true)->first();
//
//        if($user)
//            return response()->errorJson('This phone number already registered in database', 422);
//
//
//        try {
//            if ($input['whois'] == 3) { //Futbol uyinchisi
//                $whois =  $input['whois'];
//            } elseif ($input['whois'] == 4) { //Stadion rahbari
//                $whois =  $input['whois'];
//            }
//
//            $user = User::create(
//                [
//                    'phone' => $input['phone'],
//                    'password' => Hash::make($input['password']),
//                    'password_show' => $input['password'],
//                    'whois' => $whois
//                ]
//            );
//            $user->assignRole('user_player');
//
//            return $this->sms->getCode($input['phone'], $whois);
//        } catch (QueryException $exception) {
//            $data = [
//                'success' => false,
//                'errors' => $exception->getMessage(),
//                'data' => 'Ushbu telefon raqam oldindan royhatdan otgan!!!'
//            ];
//            return  response()->json($data, Response::HTTP_NOT_ACCEPTABLE);
//        }
//    }

    public function confirm(Request $request)
    {
        return $this->sms->confirmCode($request);
    }

    public function resend(Request $request)
    {
        $phone = $request->phone;
        $whois = $request->whois;
        if ($phone == '998004444444') {
            //For Test
            $responseArr['message'] = 'Telefon raqam mos kelmadi';
            return response()->json($responseArr, Response::HTTP_NOT_ACCEPTABLE);
        }
        $user = User::where('phone', $phone)->where('whois', $whois)->where('status', true)->first();
        if (!$user) {
            $data = [
                'message'=> [
                    'error' => [
                        'uz'=> 'Ma\'lumotlar bazasida mavjud emas',
                        'ru'=> 'Недоступно в базе данных',
                        'en'=> 'Not available in the database'
                    ]
                ]
            ];
            return response()->json($data, Response::HTTP_UNAUTHORIZED);
        }
        return $this->sms->getCode($phone, $whois);
    }
    /**
     * login user
     *
     * @return \Illuminate\Http\JsonResponse
     */
//    public function login(Request $request)
//    {
//        $input = $request->all();
//        $messages = [
//            'phone.required' => [
//                'uz'=> 'Telefon maydonini kiritish shart',
//                'ru'=> 'Поле телефон обязательно',
//                'en'=> 'The phone field is required'
//            ],
//            'phone.digits_between' => [
//                'uz' => 'Telefon 12 dan 12 gacha raqamdan iborat bo\'lishi kerak',
//                'ru' => 'Телефон должен содержать от 12 до 12 цифр',
//                'en' => 'The phone must be between 12 and 12 digits'
//            ]
//        ];
//        $validator = Validator::make($input, [
//            'phone' => ['required', 'numeric', 'digits_between:12,12']
//        ], $messages);
//
//        if ($validator->fails()) {
//            $responseArr = [];
//            $responseArr['message'] = $validator->errors();
//            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
//        }
//
//        $phone  = $input['phone'];
//        $whois  = $input['whois'];
//        $user = User::where('phone', $phone)->where('whois', $whois)->where('status', true)->first();
//        $data = [
//            'message'=> [
//                'error' => [
//                    'uz'=> 'Ruxsat berilmagan',
//                    'ru'=> 'Неавторизованный',
//                    'en'=> 'Unauthorized'
//                ]
//            ]
//        ];
//
//        if($user) {
//            $password = $user->sms_code;
//        } else {
//            return response()->json($data, 401);
//        }
//        $token = Auth::attempt(['phone' => $phone, 'password' => $password, 'whois' => $whois, 'status' => 1]);
//
//        if (!$token) {
//            return response()->json($data, 401);
//        }
//
//        return $this->respondWithToken($token);
//
//    }

    public function login(Request $request)
    {
        $messages = [
            'phone.required' => [
                'uz'=> 'Telefon maydonini kiritish shart',
                'ru'=> 'Поле телефон обязательно',
                'en'=> 'The phone field is required'
            ],
            'phone.digits_between' => [
                'uz' => 'Telefon 12 dan 12 gacha raqamdan iborat bo\'lishi kerak',
                'ru' => 'Телефон должен содержать от 12 до 12 цифр',
                'en' => 'The phone must be between 12 and 12 digits'
            ],
            'whois.required' => [
                'uz' => 'Whois maydonini kiritish shart',
                'ru' => 'Поле whois обязательно',
                'en' => 'The whois field is required'
            ]
        ];
        $input = $request->all();
        $validator = Validator::make($input, [
            'phone' => ['required', 'numeric', 'digits_between:12,12'],
            'whois' => ['required']
        ], $messages);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

        $phone = $input['phone'];
        $whois = $input['whois'];
        if ($phone == '998004444444') {
            //For Test
            $responseArr['message'] = 'Telefon raqam mos kelmadi';
            return response()->json($responseArr, Response::HTTP_NOT_ACCEPTABLE);
        }
        $user = User::where('phone', $phone)->where('whois', $whois)->first();

        if(!$user) {
            //Ruyhatdan uting
            $responseArr['message'] = [
                'error' => [
                    'uz' => 'Roʻyxatdan oʻtish',
                    'ru' => 'Зарегистрироваться',
                    'en' => 'Register'
                ]
            ];
            return response()->json($responseArr, Response::HTTP_NOT_ACCEPTABLE);
        }

        return $this->sms->getCode($phone, $whois);

    }
    /**
     * login user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userLogin($user)
    {
        $phone  = $user->phone;
        $whois  = $user->whois;
        $password  = $user->sms_code;

        $data = [
            'message'=> [
                'error' => [
                    'uz'=> 'Ruxsat berilmagan',
                    'ru'=> 'Неавторизованный',
                    'en'=> 'Unauthorized'
                ]
            ]
        ];
        $token = Auth::attempt(['phone' => $phone, 'password' => $password, 'whois' => $whois, 'status' => 1]);

        if (!$token) {
            return response()->json($data, 401);
        }

        return $this->respondWithToken($token);

    }

    public function resetPassword(Request $request)
    {
        $input = $request->all();

        $messages = [
            'password' => 'Password Confirmation should match the Password',
        ];
        $validator = Validator::make($input, [
            'phone' => ['required', 'numeric', 'digits_between:12,12'],
            'password' => ['required', 'string'],
            'whois' => ['required']
        ], $messages);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }

        $user = User::where('phone', $input['phone'])->where('whois', $input['whois'])->where('status', true)->where('active', true)->first();

        if(!$user)
            return response()->errorJson('Authorization Required', 422);

        Cache::put($user->phone, $input['password'], 120);

        return $this->sms->getCode($input['phone'], $input['whois']);
    }

    /**
     * Logout user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $user_id = auth()->id();
        User::whereId($user_id)->update(['api_token' => null]);
        auth()->logout();
        $data = [
            'message'=> [
                'error' => [
                    'uz'=> 'Foydalanuvchi tizimdan muvaffaqiyatli chiqdi.',
                    'ru'=> 'Пользователь успешно вышел из системы.',
                    'en'=> 'User successfully logged out.'
                ]
            ]
        ];

        return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Refresh token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get user profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $model = Auth::user();
        $model->geolocation = false;
        if($model->player) {
            $model->geolocation = true;
        }
        $users = [
            'id' => $model->id,
            'full_name' => $model->full_name,
            'phone' => $model->phone,
            'status' => $model->status,
            'active' => $model->active,
            'whois' => $model->whois,
            'birth_date' => $model->birth_date,
            'avatar' => $model->avatar,
            'tg_nickname' => $model->tg_nickname,
            'geolocation' => $model->geolocation,
        ];

        if($model->player) {
            $users['player'] = [
                'id' => $model->player->id,
                'latitude' => $model->player->latitude,
                'longitude' => $model->player->longitude,
                'region' => $model->player->region,
                'city' => $model->player->city,
                'address' => $model->player->address,
                'team_membership' => $model->player->team_membership ?? null,
                'team_captain' => $model->player->team_captain
            ];
        }

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 1 *60*24*7, // 7 day
            'user' => $users,
//            'role' => Auth::user()->getRoleNames() ? Auth::user()->getRoleNames() : null
        ]);
    }

}
