<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\StadionPhotoCollection;
use App\Http\Resources\StadionPhotoResource;
use App\Models\StadionPhoto;
use App\Traits\FileTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class StadionPhotoController extends Controller
{
    use FileTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stadion = StadionPhoto::where('stadion_id', $request->stadion_id)
            ->select('id', 'image')
            ->get();
        return response()->successJson($stadion);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'stadion_id' => ['required'],
        ]);

        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['message'] = $validator->errors();
            return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
        }
        $input = $request->all();
        $res = StadionPhoto::create([
            "stadion_id" => $input['stadion_id'],
            "image" => $this->storePhoto($request->file('photo'), 'stadiums')
        ]);

        return response()->successJson($res);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stdPhoto = StadionPhoto::find($id);
        return response()->successJson($stdPhoto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stdPhoto = StadionPhoto::find($id);

        $path_file = str_replace(config('app.url').'/storage', 'public', $stdPhoto->image);
        Storage::delete($path_file);

        $stdPhoto->image = $this->storePhoto($request->file('photo'), 'stadiums');
        $stdPhoto->save();

        return response()->successJson($stdPhoto);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $stdPhoto = StadionPhoto::findOrFail($id);

        $result = $stdPhoto->delete();
        if($result){
            $path_file = str_replace(config('app.url').'/storage', 'public', $stdPhoto->image);
            Storage::delete($path_file);

            return response()->json('Record has been deleted');
        }
    }
}
