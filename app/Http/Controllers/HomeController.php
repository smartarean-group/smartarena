<?php

namespace App\Http\Controllers;

use App\Models\TestTbl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function testt()
    {
        $row_code = 105;
//        $model = TestTbl::find(1)->update([
//            'datas' => [
//                '101' => [
//                    'homes' => 'rrrrrrrrrrrrrrr'
//                ]
//            ]
//        ]);
        $model = TestTbl::find(1)->update([
            'datas->'.$row_code.'->homes' =>'eeee',
            'datas->'.$row_code.'->one_room' =>'ttttttt',
            'datas->'.$row_code.'->two_room' =>'yyyyyyy',
            'datas->'.$row_code.'->three_room' =>'ddddddd',
            'datas->'.$row_code.'->four_room' =>'sssssss',
            'datas->'.$row_code.'->five_room' =>'aaaaaaa'
        ]);

        return 'tesst';
    }
}
