<?php

namespace App\Http\Controllers;

use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;

class SmsController extends Controller
{
    public function getCode($phone, $whois)
    {
        $code = rand(1111, 9999);
        $is_phone_in_db = User::where('phone', $phone)->where('whois', $whois)->first();

        if ($is_phone_in_db) {
            $is_phone_in_db->sms_code = $code;
            $is_phone_in_db->password = Hash::make($code);
            $is_phone_in_db->save();
        }

        $send_sms = $this->sendSms($phone, 'Smartarena mobil ilovasiga kirish kodi: ' . $code . "\noMceNCOfM2I");
        $message = [
            'uz' => "Telefon raqamga sms kod muvaffaqiyatli yuborildi",
            'ru' => "SMS-код успешно отправлен на номер телефона",
            'en' => "The SMS code was successfully sent to the phone number"
        ];
        if($send_sms){
            return response()->json(['success' => true, 'message' => $message, 'errors' => '', 'data' => null], 200);
        }

    }

    public function confirmCode($request)
    {
        $user = User::where('phone', $request->phone)->where('sms_code', $request->sms_code)->where('whois', $request->whois)->first();
        if ($user) {
            if (Hash::check($request->sms_code, $user->getAuthPassword())) {
                $user->update([
                    'status' => true,
                    'api_token' => $user->createToken(time())->plainTextToken
                ]);
            }

            $message = [
                'uz' => "Akkount paroli muvaffaqiyatli o'zgartirildi!",
                'ru' => "Пароль учетной записи успешно изменен!",
                'en' => "Account password has been changed successfully!"
            ];

            $data['user'] = [
                'id' => $user->id,
                'api_token' => $user->api_token,
                'full_name' => $user->full_name,
                'phone' => $user->phone,
                'birth_date' => $user->birth_date,
                'avatar' => $user->avatar,
                'tg_nickname' => $user->tg_nickname,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
            ];


            $result = [
                'success' => true,
                'errors' => "",
                'message' => $message,
                'data' => $data
            ];
            return response()->json($result, 200);
        } else {
            $data = [
                'message'=> [
                    'error' => [
                        'uz' => "Xatoni tasdiqlang. Telefon yoki kod noto'g'ri",
                        'ru' => "Подтвердите ошибку. Либо телефон, либо код неверный",
                        'en' => "Confirm error. Either phone or code is incorrect"
                    ]
                ]
            ];
            return response()->json($data, 400);
        }
    }
//    public function confirmCode($request)
//    {
//        $get_password = Cache::get($request->phone);
//        $user = User::where('phone', $request->phone)->where('sms_code', $request->sms_code)->first();
//
//        if ($user) {
//            if (!$get_password) {
//                $user->update([
//                    'status' => true
//                ]);
//                return response()->json('Registered Successfully', 402);
//            }
//
//            $user->update([
//                'password' => Hash::make($get_password),
//                'status' => true
//            ]);
//
//            return response()->json('Account password has been changed successfully!');
//        } else {
//            return response()->json('Confirm error. Either phone or code is incorrect', 402);
//        }
//    }


//    public function sendSms($phone, $message)
//    {
//        $sendable_message = [
//            'version' => '1.0',
//            'id' => 123232,
//            'method' => 'opersms.send',
//            'client_secret' => '2O43asOxYZqFRSeZ',
//            'params' =>
//                [
////                    'phone' => substr($phone, 1),
//                    'phone' => $phone,
//                    'message' => $message
//                ]
//        ];
//
//        $client = new Client();
//        $response = $client->post('https://sms.mehnat.uz/serve', [
//            'json' => $sendable_message
//        ]);
//
//        if($response->getStatusCode() == 200){
//            return true;
//        } else{
//            return false;
//        }
//    }

    public function sendSms($phone, $message)
    {
        $utime = time();
        $username = "smartarena";
        $secretKey = "367c87bcb16543bbc7184cfac251e176";
        $sendable_message = [
            "utime" => $utime,
            "username" => $username,
            "service" => [
                "service" => 1
            ],
            "message" => [
                "smsid" => 101,
                "phone" => $phone,
                "text" => $message
            ]
        ];
        $client = new Client();
        $response = $client->post('https://routee.sayqal.uz/sms/TransmitSMS', [
            'headers' => [
                'X-Access-Token' => $this->generateTransmitAccessToken($username, $secretKey, $utime),
            ],
            'json' => $sendable_message
        ]);

        if($response->getStatusCode() == 200){
            return true;
        } else{
            return false;
        }

    }

    protected function generateTransmitAccessToken($userName, $secretKey, $utime)
    {
        $Access = "TransmitSMS {$userName} {$secretKey} {$utime}";
        return md5($Access);
    }


}
