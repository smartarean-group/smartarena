<?php

namespace App\Http\Filter;

use App\Models\Stadion;
use App\Models\Task;
use Illuminate\Http\Request;

class StadiumsFilter
{
    private $request;
    private $stadion;
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->stadion = new Stadion();
    }

    public function selectRaw($rows = null)
    {
        if ($rows) $this->stadion->selectRaw($rows);
    }


    public function apiFilter()
    {
        if($this->request->name) {
            $this->stadion = $this->stadion->where('name', 'ilike', '%'.$this->request->name.'%');
        }

        if($this->request->region_id) {
            $this->stadion = $this->stadion->where('region_id', $this->request->region_id);
        }

        if($this->request->city_id) {
            $this->stadion = $this->stadion->where('city_id', $this->request->city_id);
        }

        if($this->request->arena_type) {
            $this->stadion = $this->stadion->whereIn('arena_type', $this->request->arena_type);
        }

        if($this->request->wear_room == 1) {
            $this->stadion = $this->stadion->where('wear_room', $this->request->wear_room);
        }

        if($this->request->toilet == 1) {
            $this->stadion = $this->stadion->where('toilet', $this->request->toilet);
        }

        if($this->request->wash_room == 1) {
            $this->stadion = $this->stadion->where('wash_room', $this->request->wash_room);
        }

        if($this->request->car_parking == 1) {
            $this->stadion = $this->stadion->where('car_parking', $this->request->car_parking);
        }

//        if ($this->request->cheapest) {
//            $this->stadion = $this->stadion->whereHas('arenas', function ($q) {
//                $q->orderBy('price', 'asc');
//            });
//        }

        return $this->stadion;
    }
}
