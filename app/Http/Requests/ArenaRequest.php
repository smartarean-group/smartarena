<?php

namespace App\Http\Requests;

use \App\Http\Requests\BaseRequest;

class ArenaRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stadion_id' => 'required',
            'name'       => 'required',
            'size'       => 'required',
            'price'      => 'required'
        ];
    }
}
