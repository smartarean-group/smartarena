<?php

namespace App\Http\Requests;

use \App\Http\Requests\BaseRequest;

class StadionRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required',
            'day_of'      => 'required',
            'open_time'   => 'required',
            'close_time'  => 'required',
            'region_id'   => 'required',
            'city_id'     => 'required',
            'address'     => 'required',
            'arena_type'  => 'required',
            'geoposition' => 'required'
        ];
    }
}
