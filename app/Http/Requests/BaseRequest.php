<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => ':attribute maydonni to‘ldirish shart',
            'numeric' => ':attribute identifikatori raqam bo‘lishi kerak.',
            'date_format' => ':attribute :format formatiga mos kelmaydi.',
            'mimes' => 'Hujjat fayli quyidagi turdagi fayl bo‘lishi kerak: pdf.',
        ];
    }

}
