<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class ApiAuthentication
{
    public function handle($request, Closure $next)
    {
        if (!$request->expectsJson()) {
            return response(['message' => 'Only JSON requests are allowed'], 406);
        }
        $token = $request->bearerToken();
        if($token) {
            $user = User::where('api_token', $token)->first();
            if ($user) {
                auth()->guard('api')->login($user);
                return $next($request);
            }
        }
        return response([
            'message' => 'Unauthenticated'
        ], 403);
    }
}
