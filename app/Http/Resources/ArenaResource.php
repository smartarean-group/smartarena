<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArenaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $result = [];
        foreach (parent::toArray($request) as $item) {
            $result[] = [
                'id' => $item['id'],
                'name' => $item['name'],
                'size' => $item['size'],
                'price' => $item['price']
            ];
        }
        return $result;
    }
}
