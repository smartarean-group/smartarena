<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StadionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $view = ($this->viewed+1);
        $this->update([
            'viewed' => $view
        ]);
        return [
            'id' => $this->id,
            'stadion_owner_id' => $this->stadion_owner_id,
            'name' => $this->name,
            'day_of' => $this->day_of,
            'open_time' => $this->open_time,
            'close_time' => $this->close_time,
            'region_id' => $this->region_id,
            'city_id' => $this->city_id,
            'address' => $this->address,
            'payment_cash' => $this->payment_cash,
            'payment_card' => $this->payment_card,
            'sunshade' => $this->sunshade,
            'lighter' => $this->lighter,
            'geoposition' => $this->geoposition,
            'area_rules' => $this->area_rules,
            'arena_type' => $this->arena_type,
            'arenas' => new ArenaResource($this->arenas),
            'wear_room' => $this->wear_room,
            'toilet' => $this->toilet,
            'wash_room' => $this->wash_room,
            'car_parking' => $this->car_parking,
            'viewed' => $this->viewed,
            'owner_info' => new UserResource($this->user),
            'stadion_photos' => new StadionPhotoResource($this->stadionPhotos)
        ];
    }
}
