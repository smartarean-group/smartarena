<?php
namespace App\Traits;
use Illuminate\Support\Facades\Storage;
trait FileTrait
{
    public function storeManual($file, $folder)
    {
        $app_url = config('app.url');
        $dirName = 'public/'.$folder;
        $file_name = pathinfo($file['filename'], PATHINFO_FILENAME).'_'.time().'.'.pathinfo($file['filename'], PATHINFO_EXTENSION);
        $path = $dirName."/".$file_name;
        if(Storage::put($path, base64_decode($file['base64']))){
            return $app_url.'/storage/'.$folder.'/'.$file_name;
        } else {
            return null;
        }
    }

    public function storePhoto($file, $folder)
    {
        $app_url = config('app.url');
        $dirName = 'public/'.$folder;
        $file_name = $file->hashName();
        if(Storage::put($dirName, $file)){
            return $app_url.'/storage/'.$folder.'/'.$file_name;
        } else {
            return null;
        }
    }
}
