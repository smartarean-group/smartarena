<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stadion extends Model
{
    use HasFactory;

    protected $table = "stadiums";
    protected $guarded = [];

    public function arenas()
    {
        return $this->hasMany(Arena::class, 'stadion_id')->where('current_price', true);
    }

    public function stadionPhotos()
    {
        return $this->hasMany(StadionPhoto::class, 'stadion_id');
    }

    public function stadionOwnerInfo()
    {
        return $this->hasOne(User::class, 'id', 'stadion_owner_id')->select(['id', 'full_name', 'phone', 'avatar', 'tg_nickname']);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'stadion_owner_id');
    }

    public function region()
    {
        return $this->hasOne(Region::class, 'id','region_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id','city_id');
    }
}
