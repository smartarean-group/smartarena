<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StadionComment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function replyComment()
    {
        return $this->hasOne(ReplyToComment::class, 'stadion_comment_id', 'id');
    }
}
